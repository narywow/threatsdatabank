﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ThDB
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private int currentPage = 1;
        private int maxPage;
        private int pageSize;
        private ThreatsDatabase threatsDB = new ThreatsDatabase();
        struct Describe
        {
            public Describe(string header, string describer)
            {
                this.Header = header;
                this.Describer = describer;
            }

            public string Header { get; set; }
            public string Describer { get; set; }
        }
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            switch (threatsDB.LoadData())
            {
                case 0:
                    {
                        break;
                    }
                case 1:
                    {
                        QuestionWindow choise = new QuestionWindow("Файл локальной базы данных поврежден!\nХотите загрузить данные?", "Загрузка данных");
                        choise.Owner = this;
                        choise.ShowDialog();

                        if (choise.Choise)
                        {
                            UpdateWindow updateWindow = new UpdateWindow(ref threatsDB);
                            updateWindow.Owner = this;
                            updateWindow.ShowDialog();
                        }
                        break;
                    }
                case 2:
                    {
                        QuestionWindow choise = new QuestionWindow("Пришло время обновиться!\nХотите обновить данные?", "Обновленние данных");
                        choise.Owner = this;
                        choise.ShowDialog();
                        if (choise.Choise)
                        {
                            UpdateWindow updateWindow = new UpdateWindow(ref threatsDB);
                            updateWindow.Owner = this;
                            updateWindow.ShowDialog();
                        }
                        break;
                    }
                case 3:
                    {
                        QuestionWindow choise = new QuestionWindow("Отсутствует локальная база данных\nХотите загрузить данные?", "Загрузка данных");
                        choise.Owner = this;
                        choise.ShowDialog();
                        if (choise.Choise)
                        {

                            UpdateWindow updateWindow = new UpdateWindow(ref threatsDB);
                            updateWindow.Owner = this;
                            updateWindow.ShowDialog();
                        }
                        break;
                    }
            }
            updateIntervalBox.Text = threatsDB.UpdateInterval.Days.ToString();
            updateIntervalBox_KeyUp(updateIntervalBox, null);
            pageSizeComboBox.SelectedIndex = 0;
            autoSafeCheckBox.IsChecked = threatsDB.AutoSafe;
        }

        public void Pagination()
        {
            List<Threat> page = new List<Threat>();
            pageNum.Text = currentPage.ToString() + " из " + maxPage.ToString();

            for (int i = 1; i <= pageSize; i++)
            {
                int num = i + (currentPage - 1) * pageSize;

                if (num > threatsDB.Threats.Count)
                {
                    break;
                }
                page.Add(threatsDB.Threats[num]);

            }
            threatsGrid.ItemsSource = page;
        }
        private void RefreshPageProperty()
        {
            if (threatsDB.Threats.Count != 0)
            {


                if (threatsDB.Threats.Count % pageSize == 0)
                {
                    maxPage = threatsDB.Threats.Count / pageSize;
                }
                else
                {
                    maxPage = threatsDB.Threats.Count / pageSize + 1;
                }
            }
            else
            {
                maxPage = 1;
            }
            if (currentPage > maxPage)
            {
                currentPage = maxPage;
            }
            pageNum.Text = (currentPage).ToString() + " из " + (maxPage).ToString();
            Pagination();
        }
        private void pageSize_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (((ComboBox)sender).SelectedItem != null)
            {
                pageSize = (((ComboBox)sender).SelectedIndex + 1) * 15;
                RefreshPageProperty();
            }
        }
        private void firstPage_Click(object sender, RoutedEventArgs e)
        {
            currentPage = 1;
            Pagination();
        }
        private void backPage_Click(object sender, RoutedEventArgs e)
        {
            if (currentPage != 1)
            {
                currentPage--;
                Pagination();
            }
        }
        private void nextPage_Click(object sender, RoutedEventArgs e)
        {
            if (currentPage != maxPage)
            {
                currentPage++;
                Pagination();
            }
        }
        private void lastPage_Click(object sender, RoutedEventArgs e)
        {
            currentPage = maxPage;
            Pagination();
        }

        private void updateButton_Click(object sender, RoutedEventArgs e)
        {
            UpdateWindow updateWindow = new UpdateWindow(ref threatsDB);
            updateWindow.Owner = this;
            updateWindow.ShowDialog();
            RefreshPageProperty();
        }
        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            if (threatsDB.SaveData())
            {
                MessageBox.Show("Данные успешно сохранены!");
            }
        }

        private void threatsGrid_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {

            if (threatsGrid.CurrentItem != null)
            {
                List<Describe> desc = new List<Describe>();

                desc.Add(new Describe("Идентификатор:", ((Threat)threatsGrid.CurrentItem).Id));
                desc.Add(new Describe("Название:", ((Threat)threatsGrid.CurrentItem).Name));
                desc.Add(new Describe("Описание:", ((Threat)threatsGrid.CurrentItem).Description));
                desc.Add(new Describe("Источник угрозы:", ((Threat)threatsGrid.CurrentItem).Source));
                desc.Add(new Describe("Объект воздействия:", ((Threat)threatsGrid.CurrentItem).Subject));
                desc.Add(new Describe("Нарушение конфиденциальности:", ((Threat)threatsGrid.CurrentItem).BreachOfConfidentiality ? "Да" : "Нет"));
                desc.Add(new Describe("Нарушение целостности:", ((Threat)threatsGrid.CurrentItem).IntegrityViolation ? "Да" : "Нет"));
                desc.Add(new Describe("Нарушение доступности:", ((Threat)threatsGrid.CurrentItem).AccessibilityViolation ? "Да" : "Нет"));

                threatInfo.ItemsSource = desc;
            }

        }

        //updateInterval
        private void updateIntervalBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (!Char.IsDigit(e.Text, 0)) e.Handled = true;
        }
        private void updateIntervalBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (!string.IsNullOrEmpty(((TextBox)sender).Text))
            {
                string daysString = ((TextBox)sender).Text;

                if (daysString.Length < 5 && daysString.Length > 0)
                {
                    if ((daysString.Length == 1 && daysString[0] == '1') || (daysString.Length > 1 && daysString[daysString.Length - 1] == '1' && daysString[daysString.Length - 2] != '1'))
                    {
                        updateIntervalText.Text = "день";
                    }
                    else if (((daysString[0] == '2' || daysString[0] == '3' || daysString[0] == '4') && daysString.Length == 1)
                        || ((daysString[daysString.Length - 1] == '2' || daysString[daysString.Length - 1] == '3' || daysString[daysString.Length - 1] == '4') && daysString.Length > 1 && daysString[daysString.Length - 2] != '1'))
                    {
                        updateIntervalText.Text = "дня";
                    }
                    else
                    {
                        updateIntervalText.Text = "дней";
                    }
                }

                threatsDB.UpdateInterval = TimeSpan.FromDays(int.Parse(daysString));
            }

        }

        //savingData
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {

            if (threatsDB.AutoSafe)
            {
                if (threatsDB.SaveData())
                {
                    MessageBox.Show("Данные успешно сохранены!");
                    Application.Current.Shutdown();
                }
            }
            else
            {
                Application.Current.Shutdown();
            }

        }

        private void autoSafeCheckBox_Click(object sender, RoutedEventArgs e)
        {
            if (((CheckBox)sender).IsChecked != null)
            {
                if (((CheckBox)sender).IsChecked == true)
                {
                    threatsDB.AutoSafe = true;
                }
                else
                {
                    threatsDB.AutoSafe = false;
                }
            }
        }

        private void updateIntervalBox_LostFocus(object sender, RoutedEventArgs e)
        {
            ((TextBox)sender).Text = threatsDB.UpdateInterval.Days.ToString();
        }
    }
}
