﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace ThDB
{
    [Serializable]
    public class Threat
    {

        private string id;
        private string name;
        private string description;
        private string source;
        private string subject;
        private bool breachOfConfidentiality;
        private bool integrityViolation;
        private bool accessibilityViolation;
        private DateTime lastUpdate;



        public string Id { get => id; set => id = value; }

        public string Name { get => name; set => name = value; }

        public string Description { get => description; set => description = value; }

        public string Source { get => source; set => source = value; }

        public string Subject { get => subject; set => subject = value; }

        public bool BreachOfConfidentiality { get => breachOfConfidentiality; set => breachOfConfidentiality = value; }

        public bool IntegrityViolation { get => integrityViolation; set => integrityViolation = value; }

        public bool AccessibilityViolation { get => accessibilityViolation; set => accessibilityViolation = value; }

        public DateTime LastUpdate { get => lastUpdate; set => lastUpdate = value; }

        public Threat(string id, string name, string description, string source, string subject, bool breachOfConfidentiality, bool integrityViolation, bool accessibilityViolation, DateTime lastUpdate)
        {
            this.id = id;
            this.name = name;
            this.description = description;
            this.source = source;
            this.subject = subject;
            this.breachOfConfidentiality = breachOfConfidentiality;
            this.integrityViolation = integrityViolation;
            this.accessibilityViolation = accessibilityViolation;
            this.lastUpdate = lastUpdate;
        }

        //public override string ToString()
        //{
        //    return $"Идентификатор:\t{id}\nНазвание:\t{name}\nОписание\t{description}\nИсточник угрозы\t{source}\nОбъект воздействия\t{subject}\nНарушение конфиденциальности\t{(breachOfConfidentiality?"Да":"Нет")}\nНарушение целостности\t{(integrityViolation ? "Да" : "Нет")}\nНарушение доступности\t{(accessibilityViolation ? "Да" : "Нет")}";
        //}
    }
}
