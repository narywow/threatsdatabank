﻿using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ThDB
{
    [Serializable]
    public class ThreatsDatabase
    {

        private Dictionary<int, Threat> threats = new Dictionary<int, Threat>();
        private TimeSpan updateInterval = TimeSpan.FromDays(1);
        private DateTime lastUpdate;
        private bool autoSafe;

        private string pathFolder = @"data\";
        private string fileName = "thrlist.xlsx";
        private string dataFileName = "data.bin";
        private string fileURL = @"https://bdu.fstec.ru/files/documents/thrlist.xlsx";

        public TimeSpan UpdateInterval { get => updateInterval; set => updateInterval = value; }
        public Dictionary<int, Threat> Threats { get => threats; set => threats = value; }
        public bool AutoSafe { get => autoSafe; set => autoSafe = value; }

        public string Update(out List<Threat>[] updateThreats)
        {
            bool isUpdate = false;


            List<string>[] data = new List<string>[9];
            updateThreats = new List<Threat>[2];
            updateThreats[0] = new List<Threat>();
            updateThreats[1] = new List<Threat>();

            DirectoryInfo dataDir;
            try
            {
                dataDir = new DirectoryInfo(pathFolder);
            }
            catch (Exception ex)
            {
                return $"Ошибка доступа к каталогу:\n{ex.Message}";
            }

            if (!dataDir.Exists)
            {
                try
                {
                    dataDir.Create();
                }
                catch (Exception ex)
                {
                    return $"Ошибка создания каталога:\n{ex.Message}";
                }

            }


            using (WebClient client = new WebClient())
            {
                try
                {
                    client.DownloadFile(fileURL, pathFolder + fileName);
                }
                catch (Exception ex)
                {

                    return $"Ошибка скачивания файла:\n{ex.Message}";
                }
            }


            Microsoft.Office.Interop.Excel.Application excelApp = new Microsoft.Office.Interop.Excel.Application();
            Microsoft.Office.Interop.Excel.Workbook workBook;
            try
            {
                workBook = excelApp.Workbooks.Open(Path.GetFullPath(pathFolder + fileName), 0, false, 5, "", "", false, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "", true, false, 0, true, false, false);
            }
            catch (Exception e)
            {
                return e.Message;
            }
            Microsoft.Office.Interop.Excel.Worksheet workSheet = (Microsoft.Office.Interop.Excel.Worksheet)workBook.Sheets[1];

            for (int i = 0; i < data.Length; i++)
            {
                Range usedColumn;
                if (i != 8)
                {
                    usedColumn = workSheet.UsedRange.Columns[i + 1];

                }
                else
                {
                    usedColumn = workSheet.UsedRange.Columns[10];
                }
                Array values = (Array)usedColumn.Cells.Value2;
                data[i] = values.OfType<object>().Select(o => o.ToString()).ToList();
            }
            excelApp.Quit();


            for (int i = 2; i <= data[8].Count; i++)
            {
                if (!int.TryParse(data[0][i], out int id))
                {
                    return "Ошибка парсинга идентефикатора угрозы!";
                }

                if (!double.TryParse(data[8][i - 1], out double dataDouble))
                {
                    return $"Ошибка парсинга даты изменения угрозы.";
                }

                DateTime lastUpdate = DateTime.FromOADate(dataDouble);
                string textId = "УБИ." + String.Format("{0:d3}", id);


                if (Threats.ContainsKey(id))
                {
                    if (!this.Threats[id].LastUpdate.Equals(lastUpdate))
                    {
                        Threat threat = new Threat(textId, data[1][i - 1], data[2][i - 1], data[3][i - 1], data[4][i - 1], !data[5][i - 1].Equals("0"), !data[6][i - 1].Equals("0"), !data[7][i - 1].Equals("0"), lastUpdate);
                        updateThreats[0].Add(Threats[id]);
                        updateThreats[0].Add(threat);
                        this.Threats.Remove(id);
                        this.Threats.Add(id, threat);
                        isUpdate = true;
                    }
                }
                else
                {
                    Threat threat = new Threat(textId, data[1][i - 1], data[2][i - 1], data[3][i - 1], data[4][i - 1], !data[5][i - 1].Equals("0"), !data[6][i - 1].Equals("0"), !data[7][i - 1].Equals("0"), lastUpdate);
                    this.Threats.Add(id, threat);
                    updateThreats[1].Add(threat);
                    isUpdate = true;
                }

            }
            lastUpdate = DateTime.Now;

            if (isUpdate)
            {
                return $"Данные успешно обновленны.\nОбновлено угроз:\t{updateThreats[0].Count / 2}\nДобавлено новых угроз:\t{updateThreats[1].Count}";
            }
            else
            {
                return "Обновлений не обнаружено";
            }
        }

        public ThreatsDatabase()
        {
        }

        public int LoadData()
        {

            if (File.Exists(pathFolder + dataFileName))
            {
                using (Stream openFileStream = File.OpenRead(pathFolder + dataFileName))
                {
                    BinaryFormatter deserializer = new BinaryFormatter();
                    ThreatsDatabase temp = null;
                    try
                    {
                        temp = (ThreatsDatabase)deserializer.Deserialize(openFileStream);
                    }
                    catch
                    {
                        return 1;
                    }
                    finally
                    {
                        openFileStream.Close();
                    }
                    Threats = temp.Threats;
                    UpdateInterval = temp.UpdateInterval;
                    lastUpdate = temp.lastUpdate;
                    AutoSafe = temp.AutoSafe;
                }
                if (UpdateInterval.CompareTo(DateTime.Now - lastUpdate) <= 0)
                {
                    return 2;
                }
                else
                {
                    return 0;
                }
            }
            else
            {
                return 3;
            }
        }
        public bool SaveData()
        {
            DirectoryInfo dataDir = null;
            try
            {
                dataDir = new DirectoryInfo(pathFolder);
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Ошибка доступа к каталогу:\n{ex.Message}");
            }

            if (!dataDir.Exists)
            {
                try
                {
                    dataDir.Create();
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"Ошибка создания каталога:\n{ex.Message}");
                }

            }
            using (Stream SaveFileStream = File.Create(pathFolder + dataFileName))
            {
                BinaryFormatter serializer = new BinaryFormatter();

                try
                {
                    serializer.Serialize(SaveFileStream, this);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    return false;
                }
                finally
                {
                    SaveFileStream.Close();
                }
            }
            return true;
        }

    }
}
