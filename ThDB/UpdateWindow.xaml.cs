﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
namespace ThDB
{
    /// <summary>
    /// Логика взаимодействия для UpdateWindow.xaml
    /// </summary>
    /// 


    public partial class UpdateWindow : Window
    {
        private ThreatsDatabase threats;
        public UpdateWindow(ref ThreatsDatabase threats)
        {
            InitializeComponent();
            this.threats = threats;
            updateInfo.Text = "Загрузка данных...";
            Thread newThread = new Thread(new ThreadStart(Update));
            newThread.Start();

        }
        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Update()
        {
            List<Threat>[] updatedThreats;
            string info = threats.Update(out updatedThreats);
            Dispatcher.Invoke(() => updateInfo.Text = info);
            if (info.Contains("Данные успешно обновленны."))
            {
                Dispatcher.Invoke(() => updateGrid.ItemsSource = updatedThreats[0]);
                Dispatcher.Invoke(() => newGrid.ItemsSource = updatedThreats[1]);
            }
        }
    }
}
