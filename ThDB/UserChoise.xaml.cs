﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ThDB
{
    /// <summary>
    /// Логика взаимодействия для UserChoise.xaml
    /// </summary>
    public partial class QuestionWindow : Window
    {
        public string DisagreeButtonName { set; get; } = "Нет";
        public string AgreeButtonName { set; get; } = "Да";
        public bool Choise { get; set; }

        
        public QuestionWindow(String question, String title)
        {
            InitializeComponent();
            Title = title;
            questionTextBlock.Text = question;
            agreeButton.Content = AgreeButtonName;
            disagreeButton.Content = DisagreeButtonName;
        }


        private void agreeButton_Click(object sender, RoutedEventArgs e)
        {
            Choise = true;
            this.Close();
        }

        private void disagreeButton_Click(object sender, RoutedEventArgs e)
        {
            Choise = false;
            this.Close();
        }

    }
}
